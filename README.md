


## Synvata interview.
Thank you for interviewing at Synvata! We appreciate you taking the time to complete this exercise. 

## Getting Started

The only requirement is that you use Java 8+ and Spring boot.   
Don't worry too much about error handling or validations.   
The goal here is to assesss your code structure and design.   

## The problem

As an investor I would like a tool I can offer my technical teammates where we would be able to pull from SEC.gov any public data point for any institution we are analyzing to invest in.
This tool should take the SEC institution identification number (CIK number), the quarter index (the repository of submitted reports that occurred in that quarter), and the list of data points I'm interested in.

FYI: SEC is an independent US agency, that is primarily in charge of enforcing federal securities laws and regulating securities industry/stock markets.
https://www.sec.gov/

## Instructions

Create a program (CLI tool) that will take a CIK number, a quarter index, and a list of fields.   
Example:   
``
cik=1109242 quarterIndex=2019-12-31 fields=Assets,Deposits,NoninterestIncome
``

Given these three parameters (all required) the program should access, pull, and process data from the SEC Edgar website quarterly index.   
You can find the year to quarterly index at this URL:   
https://www.sec.gov/Archives/edgar/full-index/

Then, you should access the file xbrl.idx which provides the list of all institutions that reported data for quarter.   
Here is an example:    
For 2019-12-31, the URL would be:   
https://www.sec.gov/Archives/edgar/full-index/2019/QTR4/xbrl.idx

Please note that this file is a pipe-separated value document where each column represents a piece of relevant information that you will use to complete this exercise.   
**xbrl.idx contains the following columns:**     


    CIK|Company Name|Form Type|Date Filed|Filename

Please note that we are not interested in all possible form types.  
You should just lookup for data points in **10-Q, 10-K** forms (Form Type column).

![xbrl.idx example](image1.png)

After grabbing the fileName (last column), you will access it by prepending the following URL https://www.sec.gov/Archives/    
Ex: https://www.sec.gov/Archives/edgar/data/1109242/0001628280-19-012170.txt


With that at hand, you will first identify which reporting quarter this document belongs to, by accessing the tag 
**dei:DocumentPeriodEndDate** (2019-06-30 in this case)   

![documentPeriodEndDate](image2.png)


and then you will lookup for the fields requested

![fieldExample](image3.png)

- Note 1: There can be many entries for that field in the document. The program must select the one that matches the document’s reporting quarter (2019-06-30 in this case). You can identify the quarter a data point belongs to by grabbing the contextRef attribute and looking up for a node identified by that id. As you can see in the image above, FI2019Q2 points to another XML declaration and contains a child node xbrli:instant (see the xml fragment below). You must use the **xbrli:instant** attribute and not rely on the id string ("FI2019Q2") to derive the quarter as it is unpredictable.

![relationshipBetweenDataPointAndItsQuarter](image4.jpg)

- Note 2: You should only look for data points with the scope **us-gaap**. In other words, if the fields requested are Assets, Deposits, NoninterestIncome it means you would look for us-gaap:Assets, us-gaap:Deposits, us-gaap:NoninterestIncome. 

   
    


### **Expected output:** The program should print a JSON string with the following format/data:   

    [
        {
            "formType": "10-Q",
            "dateFiled": "2019-10-04",
            "fileUrl": "https://www.sec.gov/Archives/edgar/data/1109242/0001628280-19-012170.txt",
            "reportingQuarter": "2019-06-30",
            "dataPoints": {
                "us-gaap:Assets": 123,
                "us-gaap:Deposits": 456,
                "us-gaap:NoninterestIncome": 789
            }
        },
        {
            "formType": "10-Q",
            "dateFiled": "2019-11-12",
            "fileUrl": "https://www.sec.gov/Archives/edgar/data/1109242/0001628280-19-013865.txt",
            "reportingQuarter": "2019-09-30",
            "dataPoints": {
                "us-gaap:Assets": 101112,
                "us-gaap:Deposits": 131415,
                "us-gaap:NoninterestIncome": 161718
            }
        }
    ]

# Have a good time!